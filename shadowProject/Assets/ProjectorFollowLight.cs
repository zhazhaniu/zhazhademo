﻿using UnityEngine;
using System.Collections;

public class ProjectorFollowLight : MonoBehaviour {

	public Light light = null;
	public Transform center = null;
	public Transform projector = null;

	[SerializeField]
	protected float lastRot = 0f;

	// Use this for initialization
	void Start () {
		rotateCamAndProjector ();
	}
	
	// Update is called once per frame
	void Update () {
		rotateCamAndProjector ();
	}

	void rotateCamAndProjector()
	{
		Vector3 lightRot = light.transform.rotation.eulerAngles;
		transform.RotateAround (center.position, center.up, lastRot - lightRot.y);
		projector.transform.RotateAround (center.position, center.up, lastRot - lightRot.y);
		lastRot = lightRot.y;
	}
}
