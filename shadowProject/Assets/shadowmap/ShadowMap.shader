﻿Shader "Custom/ShadowMap" {
	Properties {
		_MainTex("_MainTex", 2D) = "white" {}
		_DepthTex("DepthTex", 2D) = "white" {}
	}
	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _DepthTex;
			
			float4x4 _worldToCamera;
			float4x4 _cameraProjection;

			
			struct v2f {
				float4 shadowUV : TEXCOORD0;
				float4 vertex : TEXCOORD1;
				float4 uv : TEXCOORD2;
				float4 pos : SV_POSITION;
			};
				
			v2f vert (appdata_base i)
			{
				v2f o;
				float4 pos = mul(_Object2World, i.vertex);
				pos = mul(_worldToCamera, pos);
				pos = mul(_cameraProjection, pos);
				o.shadowUV = pos;
				o.vertex = i.vertex;
				o.uv = i.texcoord;
				o.pos = mul(UNITY_MATRIX_MVP, i.vertex);
				return o;
			}

			fixed4 frag(v2f i) : COLOR
			{
				float z = i.pos.z / i.pos.w;
				float depth = 0;
				float3 norm;
				DecodeDepthNormal(tex2D(_DepthTex, i.shadowUV.xy/i.shadowUV.w), depth, norm);
				fixed4 result = tex2D(_MainTex, i.uv);
				if (z < depth)
				{
					result.rgb = fixed3(0, 0, 0);
				}
				
				return result;
			}
			
			ENDCG
		}
	}
	FallBack "Diffuse"
}
