﻿using UnityEngine;
using System.Collections;

public class CamGenerateDepth : MonoBehaviour 
{

	public Light directionLight = null;
	public Material shadowMat = null;

	protected Camera cam = null;

	// Use this for initialization
	void Start () 
	{
		transform.position = directionLight.transform.position;
		transform.rotation = directionLight.transform.rotation;

		cam = GetComponent<Camera> ();
		cam.depthTextureMode = DepthTextureMode.DepthNormals;

		shadowMat.SetMatrix ("_worldToCamera", cam.worldToCameraMatrix);
		shadowMat.SetMatrix ("_cameraProjection", cam.projectionMatrix);
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = directionLight.transform.position;
		transform.rotation = directionLight.transform.rotation;
	}
 
}
