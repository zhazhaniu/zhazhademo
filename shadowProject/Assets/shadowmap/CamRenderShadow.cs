﻿using UnityEngine;
using System.Collections;

public class CamRenderShadow : MonoBehaviour 
{	
	public Camera shadowCam = null;
	public CamGenerateDepth genDepth = null;

	// Use this for initialization
	void Start () 
	{
		genDepth = shadowCam.GetComponent<CamGenerateDepth> ();
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	
}
