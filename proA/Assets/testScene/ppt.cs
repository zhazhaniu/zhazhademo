﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ppt : MonoBehaviour {
    // bind gameobject
    public GameObject image = null;
    public GameObject dialogue = null;
    // time show ppt
    private float t_start;
    private float t_end;
    private int nIndex = 0;
    const int MAX_PPT = 3;      // 最大的幻灯片张数(第一张不需要播放)
    const int INTERNAL = 2;     // 幻灯片间隔(单位为s)
	// Use this for initialization
	void Start () {
        t_start = Time.time;
        Debug.Log("start ... ");
        nIndex = 0;
	}
	
	// Update is called once per frame
	void Update () {
        t_end = Time.time;
        float tOff = t_end - t_start;
        if (tOff > INTERNAL && nIndex < MAX_PPT)
        {
            Debug.Log("log ...");
            t_start = Time.time;
            ++nIndex;
            // notify: 第一张不用播放
            string strName = "ppt/p" + (nIndex+1);
            Debug.Log(strName);
            Object obj = Resources.Load(strName);
            image.GetComponent<RawImage>().texture = obj as Texture;
            
            if (nIndex >= MAX_PPT)
            {
                Invoke("startDialogue", 3f);
            }
        }
        else
        {
        }
        
	}

    public void startDialogue()
    {
        dialogue.SetActive(true);
        ShowDialogue sd = dialogue.GetComponent<ShowDialogue>();
        sd.onFinished = onDialogueFinished;
        sd.currentIndex = 1;
        sd.showCurrentDialogue();
    }

    public void onDialogueFinished()
    {
        Application.LoadLevel(2);
    }
}
