﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DataST;

public class ShowDialogue : MonoBehaviour {

    public GameObject role1 = null;
    public GameObject role2 = null;
    public GameObject name = null;
    public GameObject text = null;
    public GameObject showText = null;

    public int currentIndex = -1;

    public static Dictionary<string, Object> resCache = new Dictionary<string, Object>();
    public System.Action onFinished = null;

    void Awake()
    {
        DataST.Data.init();
    }

	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void showCurrentDialogue()
    {
        var conf = getCurrentConfig();
        if (conf != null)
        {
            name.GetComponent<Text>().text = conf.name;
            role1.SetActive(false);
            role2.SetActive(false);
            if (conf.image1 != "")
            {
                role1.SetActive(true);
                role1.GetComponent<RawImage>().texture = (getRes("dialogue/" + conf.image1) as Texture);
                showText.GetComponent<Image>().overrideSprite = (getSpriteRes("dialogue/dk1") as Sprite);
                
            }
            if (conf.image2 != "")
            {
                role2.SetActive(true);
                role2.GetComponent<RawImage>().texture = (getRes("dialogue/" + conf.image2) as Texture);
                showText.GetComponent<Image>().overrideSprite = (getSpriteRes("dialogue/dk2") as Sprite);
            }

            if (conf.bg != "")
            {
                gameObject.GetComponent<RawImage>().texture = (getRes("dialogue/" + conf.bg) as Texture);
            }

            text.GetComponent<Text>().text = conf.text;
            
        }
        else
        {
            gameObject.SetActive(false);
            if (onFinished != null)
            {
                onFinished();
                onFinished = null;
            }
        }
    }

    public void nextDialogue()
    {
        var conf = getCurrentConfig();
        currentIndex = conf.nextID;
        showCurrentDialogue();
    }

    public Object getRes(string path)
    {
        if (resCache.ContainsKey(path))
        {
            return resCache[path];
        }

        Object obj = Resources.Load(path);
        if (obj)
        {
            resCache.Add(path, obj);
        }

        return obj;
    }

    public Object getSpriteRes(string path)
    {
        if (resCache.ContainsKey(path))
        {
            return resCache[path];
        }

        Object obj = Resources.Load(path, typeof(Sprite));
        if (obj)
        {
            resCache.Add(path, obj);
        }

        return obj;
    }

    dialog_ST.Node getCurrentConfig()
    {
        if (DataST.Data.dialog._data.ContainsKey(currentIndex))
        {
            return DataST.Data.dialog._data[currentIndex];
        }
        return null;
    }
}
