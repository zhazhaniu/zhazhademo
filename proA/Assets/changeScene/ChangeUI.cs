﻿using UnityEngine;
using System.Collections;

public class ChangeUI : MonoBehaviour {

    public GameObject me = null;
    public GameObject target = null;
    public GameObject show_button = null;
    public GameObject dialogue = null;
    public GameObject bigmap = null;
    private bool _change_flag = false;
    public GameObject endPanel = null;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void onAttack(GameObject tar)
    {
        if (me && target)
        { 
            tar.SetActive(false);
            show_button.SetActive(true); 
            target.SetActive(_change_flag);
            me.SetActive(!_change_flag);
            _change_flag = !_change_flag; 
        }
    }
    //结束换装
    public void onEndChange( )
    {
        if (show_button)
        { 
            show_button.SetActive(false);
            var dlg = dialogue.GetComponent<ShowDialogue>();
            dlg.currentIndex = 13;
            dlg.onFinished = onDialogueFinished;
            dialogue.SetActive(true);
            dlg.showCurrentDialogue();
            
        }
    }
    //打开cjsz map 
    public void openCjszMap()
    {
        //bigmap.SetActive(false);
        var dlg = dialogue.GetComponent<ShowDialogue>();
        dlg.currentIndex = 16;
        dlg.onFinished = onDialogue2Finished;
        dialogue.SetActive(true);
        dlg.showCurrentDialogue();
    }
    void onDialogueFinished()
    {
        bigmap.SetActive(true);
    }

    void onDialogue2Finished()
    {
        endPanel.SetActive(true);
    }

    void enemyAttack()
    {
        if (me && target)
        {
            target.GetComponent<Animation>().Play("attack1");
            Invoke("enemyAttack_1", 1f);
        }
    }

    void enemyAttack_1()
    {
        var behit01 = me.transform.Find("behit01");
        behit01.gameObject.SetActive(false);
        behit01.gameObject.SetActive(true);
    }
}
