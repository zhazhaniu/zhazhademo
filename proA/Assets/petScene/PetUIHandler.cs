﻿using UnityEngine;
using System.Collections;

public class PetUIHandler : MonoBehaviour {
	public GameObject egg = null;
	public GameObject pet = null;
    public GameObject cloth = null;
    public GameObject movie = null;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void onBrokeEgg()
	{
		egg.SetActive (false);
        movie.SetActive(true);
        movie.GetComponent<UMovie>().onFinished = onGainBikac;
	}

    public void onGainBikac()
    {
        movie.SetActive(false);
        pet.SetActive(true);
    }

    public void onGainCloth()
    {
        pet.SetActive(false);
        cloth.SetActive(true);
    }

	public void onBack()
	{
		Application.LoadLevel(4);
	}
}
