
    Shader "Spine/BonesWithNormal" {
        Properties {
		_Color ("Color", Color) = (0.5,0.5,0.5,0.5)
		_MainTex ("Particle Texture", 2D) = "white" {}
		//_Cutoff ("Shadow alpha cutoff", Range(0,1)) = 0.1
        //_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
        //_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
		_NormalRate ("NormalRate", Range (0, 0.2)) = 0.05
        _BumpMap ("Normalmap", 2D) = "bump" {}
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend SrcAlpha OneMinusSrcAlpha
	AlphaTest Greater .01
	ColorMask RGB
	
	Lighting Off Cull Off ZTest Always ZWrite Off Fog { Mode Off }

	SubShader {
		Pass {
			Tags {"LightMode" = "ForwardBase"}                      // This Pass tag is important or Unity may not give it the correct light information.

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_particles
			
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"

			sampler2D _MainTex;
			fixed4 _Color;
            sampler2D _BumpMap;
            half _Shininess;
			
			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				#ifdef SOFTPARTICLES_ON
				float4 projPos : TEXCOORD1;
				#endif

				float3  viewDir     : TEXCOORD2;
                float3  lightDir    : TEXCOORD3;
                LIGHTING_COORDS(4,5)  
			};
			
			float4 _MainTex_ST;
            fixed4 _SpecColor;
            fixed4 _LightColor0; // Colour of the light used in this pass.
            fixed _Cutoff;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				#ifdef SOFTPARTICLES_ON
				o.projPos = ComputeScreenPos (o.vertex);
				COMPUTE_EYEDEPTH(o.projPos.z);
				#endif
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				
				
				o.viewDir = WorldSpaceViewDir(v.vertex);
                o.lightDir = WorldSpaceLightDir(v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o);                 // Macro to send shadow  attenuation to the fragment shader.
				return o;
			}

			sampler2D_float _CameraDepthTexture;
			float _NormalRate;

			
			fixed4 frag (v2f i) : SV_Target
			{			
				float3 lightColor = _LightColor0.rgb;
                i.viewDir = normalize(i.viewDir);
                i.lightDir = normalize(i.lightDir);
                       
                fixed atten = LIGHT_ATTENUATION(i); // Macro to get you the combined shadow  attenuation value.
     
                fixed4 tex = tex2D(_MainTex, i.texcoord);
                fixed gloss = tex.a;
                tex *= _Color;
						
                fixed3 normal = UnpackNormal(tex2D(_BumpMap, i.texcoord));
				normal = UnityObjectToWorldNormal(normal);
                half3 h = normalize(i.lightDir + i.viewDir);
                fixed diff = dot(normal, i.lightDir);
                float nh = saturate(dot (normal, h));
                //float spec = pow(nh, _Shininess * 128.0) * gloss;
                       
                fixed4 c;
                //c.rgb = UNITY_LIGHTMODEL_AMBIENT.rgb * 2 * tex.rgb;         // Ambient term. Only do this in Forward Base. It only needs calculating once.
                //c.rgb += (tex.rgb * _LightColor0.rgb * diff + _LightColor0.rgb * _SpecColor.rgb * spec) * (atten * 2); // Diffuse and specular.
                //c.a = tex.a + _LightColor0.a * _SpecColor.a * spec * atten;	
				
				return 2.0f * i.color * _Color * tex2D(_MainTex, i.texcoord) * (1-_NormalRate) + _NormalRate * (diff *_LightColor0 ) ;
			}
			ENDCG 
		}
	}	
}
        FallBack "Transparent/Cutout/Diffuse"    // Use VertexLit's shadow caster/receiver passes.
   }