/******************************************************************************
 * Spine Runtimes Software License
 * Version 2.3
 * 
 * Copyright (c) 2013-2015, Esoteric Software
 * All rights reserved.
 * 
 * You are granted a perpetual, non-exclusive, non-sublicensable and
 * non-transferable license to use, install, execute and perform the Spine
 * Runtimes Software (the "Software") and derivative works solely for personal
 * or internal use. Without the written permission of Esoteric Software (see
 * Section 2 of the Spine Software License Agreement), you may not (a) modify,
 * translate, adapt or otherwise create derivative works, improvements of the
 * Software or develop new applications using the Software or (b) remove,
 * delete, alter or obscure any trademarks or any copyright, trademark, patent
 * or other intellectual property or proprietary rights notices on or in the
 * Software, including any copy thereof. Redistributions in binary or source
 * form must include this license and terms.
 * 
 * THIS SOFTWARE IS PROVIDED BY ESOTERIC SOFTWARE "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL ESOTERIC SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Spine;

public class Menus {
	[MenuItem("Assets/Create/Spine Atlas")]
	static public void CreateAtlas () {
		CreateAsset<AtlasAsset>("New Atlas");
	}
	
	[MenuItem("Assets/Create/Spine SkeletonData")]
	static public void CreateSkeletonData () {
		CreateAsset<SkeletonDataAsset>("New SkeletonData");
	}
	
	static private void CreateAsset <T> (String name) where T : ScriptableObject {
		var dir = "Assets/";
		var selected = Selection.activeObject;
		if (selected != null) {
			var assetDir = AssetDatabase.GetAssetPath(selected.GetInstanceID());
			if (assetDir.Length > 0 && Directory.Exists(assetDir))
				dir = assetDir + "/";
		}
		ScriptableObject asset = ScriptableObject.CreateInstance<T>();
		AssetDatabase.CreateAsset(asset, dir + name + ".asset");
		AssetDatabase.SaveAssets();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = asset;
	}

	[MenuItem("GameObject/Create Other/Spine SkeletonRenderer")]
	static public void CreateSkeletonRendererGameObject () {
		GameObject gameObject = new GameObject("New SkeletonRenderer", typeof(SkeletonRenderer));
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = gameObject;
	}

	[MenuItem("GameObject/Create Other/Spine SkeletonAnimation")]
	static public void CreateSkeletonAnimationGameObject () {
		GameObject gameObject = new GameObject("New SkeletonAnimation", typeof(SkeletonAnimation));
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = gameObject;
	}

    [MenuItem("Assets/batchCreateSpineData")]
    static public void BatchCreateSpineData(  )
    {
        
        Debug.Log("Selected Transform is on "  + Selection.activeObject.name+ ".");
        string dirName = "";
        string spineFileName = "skeleton";
        Debug.Log(spineFileName);
        Debug.Log(dirName);
        Debug.Log("dir name ");

        string textureName = dirName + spineFileName + ".png";
        string jsonFileName = dirName + spineFileName + ".json.txt";
        string atlasFileName = dirName + spineFileName + ".atlas.txt";

        Material mat;
        ///1、 创建材质，并指贴图和shader
        {
            Shader shader = Shader.Find("Unlit/Alpha_zorro");
            mat = new Material(shader);
            Texture tex = AssetDatabase.LoadAssetAtPath(textureName, typeof(Texture)) as Texture;
            mat.SetTexture("_MainTex", tex);
            AssetDatabase.CreateAsset(mat, dirName + spineFileName + ".mat");
            AssetDatabase.SaveAssets();
        }

        ///2、 创建atlas，并指xx
        AtlasAsset m_AtlasAsset = AtlasAsset.CreateInstance<AtlasAsset>();
        AssetDatabase.CreateAsset(m_AtlasAsset, dirName + spineFileName + ".asset");
        Selection.activeObject = m_AtlasAsset;

        TextAsset textAsset = AssetDatabase.LoadAssetAtPath(atlasFileName, typeof(TextAsset)) as TextAsset;
        m_AtlasAsset.atlasFile = textAsset;
        m_AtlasAsset.materials = new Material[1];
        m_AtlasAsset.materials[0] = mat;
        AssetDatabase.SaveAssets();


        ///3、 创建SkeletonDataAsset，并指相关
        SkeletonDataAsset m_skeltonDataAsset = SkeletonDataAsset.CreateInstance<SkeletonDataAsset>();
        AssetDatabase.CreateAsset(m_skeltonDataAsset, dirName + spineFileName + " AnimationData.asset");
        Selection.activeObject = m_skeltonDataAsset;

        m_skeltonDataAsset.atlasAssets[0] = m_AtlasAsset;
        TextAsset m_jsonAsset = AssetDatabase.LoadAssetAtPath(jsonFileName, typeof(TextAsset)) as TextAsset; 
        m_skeltonDataAsset.skeletonJSON = m_jsonAsset;
        AssetDatabase.SaveAssets();


        /// 创建场景物件
        GameObject gameObject = new GameObject(spineFileName, typeof(SkeletonAnimation));
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = gameObject;

        SkeletonAnimation m_skelAnim = gameObject.GetComponent<SkeletonAnimation>();
        m_skelAnim.skeletonDataAsset = m_skeltonDataAsset;
    }

}
