﻿using System;
using System.Collections.Generic;
using System.Collections;
using DataST;

namespace DataST
{

public class message_ST
{
    public class Node
    {

		public int id;
		public string text;

    }

    public Node this[int id]
    {
        get
        {
            if( _data.ContainsKey(id) )
            {
                return _data[id];
            }
            return null;
        }
    }

	public Dictionary<int, Node> _data;
}

}
