﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace DataST
{
    public interface IExprData
    {
        // Indexer declaration:
        double this[string index]
        {
            get;
            set;
        }
    }

    public class expr
    {
        public delegate double exprFunction(IExprData ob);
        public exprFunction func;
        public List<string> depends;
        public string desc;
    }


    public partial class Data
    {
        // 按照独特方式初始化数据
        public delegate void InitDataFunc();
        public static List<InitDataFunc> _initDataFuncs;
        // 自动初始化列表
        public class AutoInitData
        {
            public AutoInitData(InitDataFunc func)
            {
                addInitDataFunc( func );
            }
        }
        public static void addInitDataFunc(InitDataFunc func)
        {
            if (_initDataFuncs == null )
            {
                _initDataFuncs = new List<InitDataFunc>();
            }
            _initDataFuncs.Add(func);
        }
        public static void init()
        {
            for (int i = 0; i < _initDataFuncs.Count; i++)
            {
                _initDataFuncs[i]();
            }
        }
    }

}

