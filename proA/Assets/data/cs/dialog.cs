﻿using System;
using System.Collections.Generic;
using System.Collections;
using DataST;

namespace DataST
{

public partial class Data
{
    public static dialog_ST dialog = new dialog_ST
    {
        _data = new Dictionary<int, dialog_ST.Node>()
    };
 


    public static AutoInitData dialog_autoinit0 = new AutoInitData(dialog_init0);
    public static void dialog_init0()
    {
		dialog._data[1] = new dialog_ST.Node(){ bg="bg1", nextID=2, name="董龙", text="我董龙无心为难你们，乖乖把红衣魔女和长孙染情的孽种交出来。否则，别怪我的刀太快！", image2="dl", image1="", id=1, };
		dialog._data[2] = new dialog_ST.Node(){ bg="bg1", nextID=3, name="村长", text="主角你快走，别管我们。你挂了这游戏还怎么玩！", image2="", image1="cz2", id=2, };
		dialog._data[3] = new dialog_ST.Node(){ bg="bg1", nextID=4, name="主角", text="村长Σ( ° △ °|||)︴……", image2="zj", image1="", id=3, };
		dialog._data[4] = new dialog_ST.Node(){ bg="bg1", nextID=5, name="董龙", text="小的们，给我杀！", image2="dl", image1="", id=4, };
		dialog._data[5] = new dialog_ST.Node(){ bg="bg4", nextID=-1, name="李复", text="尔等何人？敢在稻香村里撒野！且过了老夫这关！", image2="", image1="lf", id=5, };
		dialog._data[6] = new dialog_ST.Node(){ bg="bg2", nextID=7, name="董龙", text="我董龙不是什么大人物，区区小卒尔，稻香村保得了他一时，未必保得了一世！", image2="dl", image1="", id=6, };
		dialog._data[7] = new dialog_ST.Node(){ bg="bg2", nextID=8, name="李复", text="今日放你回去，日后望你好自为之。", image2="", image1="lf", id=7, };
		dialog._data[8] = new dialog_ST.Node(){ bg="bg2", nextID=9, name="主角", text="李伯伯，他们究竟为何抓我？", image2="zj", image1="", id=8, };
		dialog._data[9] = new dialog_ST.Node(){ bg="bg2", nextID=10, name="李复", text="其实你并非孤儿，也是时候去寻找你的父母了。", image2="", image1="lf", id=9, };
		dialog._data[10] = new dialog_ST.Node(){ bg="bg2", nextID=11, name="主角", text="我的父母？", image2="zj", image1="", id=10, };
		dialog._data[11] = new dialog_ST.Node(){ bg="bg2", nextID=12, name="李复", text="嗯。事不宜迟，现在就出发吧。我有两样东西送你，可保护你在路上的安全。", image2="", image1="lf", id=11, };
		dialog._data[12] = new dialog_ST.Node(){ bg="bg2", nextID=-1, name="李复", text="这颗千年古鸡蛋，可召唤出护身灵兽，不过具体是什么兽要看缘分。你不妨试试吧！", image2="", image1="lf", id=12, };
		dialog._data[13] = new dialog_ST.Node(){ bg="bg2", nextID=14, name="李复", text="甚好，这样子我就放心多了。如今江湖未稳，我已嘱咐藏剑山庄收你做弟子，你当前往好好学习本领！", image2="", image1="lf", id=13, };
		dialog._data[14] = new dialog_ST.Node(){ bg="bg2", nextID=15, name="村长", text="是啊是啊，记得没事就回来看看！", image2="zj", image1="", id=14, };
		dialog._data[15] = new dialog_ST.Node(){ bg="bg2", nextID=-1, name="主角", text="知道了，稻香村就交给你们了……", image2="", image1="cz2", id=15, };
		dialog._data[16] = new dialog_ST.Node(){ bg="bg3", nextID=-1, name="叶英", text="是李先生的义子吧，既有缘入我藏剑一门，便喊我一声师父吧。", image2="yy", image1="", id=16, };

    }



}

}
