﻿using UnityEngine;
using System.Collections;

public class OnKeyboard : MonoBehaviour {
	public GameObject[] layers = null;
	public float speed = 1f;
    public float angleRange = 0.5f;
    public float accAngle = 0f;

	public SkeletonAnimation ani = null;

	// Use this for initialization
    void Awake()
    {
        //Screen.SetResolution(720, 1280, false);
        //Screen.SetResolution(540, 960, false);
    }
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
            turnRight(1);
		}

		if (Input.GetKey (KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
            turnLeft(1);
		}

	}

    public void turnLeft(float speedMulti)
    {
        if (accAngle > angleRange)
        {
            return;
        }
        float angle = speed * Time.deltaTime * speedMulti;
        accAngle += angle;
        transform.Rotate(new Vector3(0, angle, 0));
        turnBack(angle);
        if (ani != null)
        {
            ani.AnimationName = "animation2";
        }
    }

    public void turnRight(float speedMulti)
    {
        if (accAngle < -angleRange)
        {
            return;
        }
        float angle = -speed * Time.deltaTime * speedMulti;
        accAngle += angle;
        transform.Rotate(new Vector3(0, angle, 0));
        turnBack(angle);
        if (ani != null)
        {
            ani.AnimationName = "animation2";
        }
    }

    public void turnBack(float camRotate)
	{
		foreach (var obj in layers) {
			var pos = obj.transform.position;
			pos.x += camRotate * pos.z / 3f;
			obj.transform.position = pos;

			//var mat = obj.GetComponent<Renderer> ().material;
			//if (mat.HasProperty("_Angle"))
			//{
				//mat.SetFloat ("_Angle", mat.GetFloat("_Angle") - camRotate * pos.z /230 );
			//}
		}
	}


}
