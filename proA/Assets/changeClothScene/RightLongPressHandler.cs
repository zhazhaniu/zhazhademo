﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class RightLongPressHandler : MonoBehaviour, IPointerDownHandler
{
    private bool isPointDown = false;
    protected OnKeyboard kb = null;
    // Use this for initialization
    void Start()
    {
        kb = Camera.main.GetComponent<OnKeyboard>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPointDown)
        {
            kb.turnRight(2);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isPointDown = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPointDown = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isPointDown = false;
    }
}
