﻿using UnityEngine;
using System.Collections;

public class DragRotate : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private Vector3 screenPoint;
    private Vector3 offset;
    private Vector3 lastPos = Vector3.zero;

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        lastPos = screenPoint;
    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;

        var kb = Camera.main.GetComponent<OnKeyboard>();
        if (lastPos.x < curScreenPoint.x)
        {
            kb.turnLeft(2);
        }
        else if (lastPos.x > curScreenPoint.x)
        {
            kb.turnRight(2);
        }
        lastPos = curScreenPoint;
    }

    void OnMouseUp()
    {
        lastPos = Vector3.zero;
    }
}
