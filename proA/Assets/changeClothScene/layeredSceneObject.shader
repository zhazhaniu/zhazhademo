Shader "Custom/layeredSceneObject" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Angle ("Angle", Range(-1,1)) = 0.0
		//_Cutoff("Cutoff", Range(0, 1)) = 0
		_YFloatRate("Y Float Rate", float) = 0
	}
	SubShader {
		Tags { "Queue"="Transparent"  "RenderType"="Transparent" }
		LOD 200

		ZWrite off
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert alpha

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 2.0

		float4x4 getZMatrix(float angle)
		{
			float4x4 m = {
				float4(1, angle, 0, 0),
				float4(angle, 1, 0, 0),
				float4(0, 0, 1, 0),
				float4(0, 0, 0, 1)
			};

			return m;
		}

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		float _Angle;
		float _YFloatRate;

		void vert (inout appdata_full v) 
		{
			v.vertex.y += 0.2 * sin((v.vertex.x + v.vertex.z)  + _Time.w) * _YFloatRate;
			v.vertex.y += 0.3 * sin((v.vertex.x - v.vertex.z) + _Time.y) * _YFloatRate;
			v.vertex.y += 0.1 * sin((v.vertex.x) + _Time.x) * _YFloatRate;
			v.vertex.y += 0.2 * sin((v.vertex.z) + _Time.z) * _YFloatRate;
			float4x4 m = getZMatrix(_Angle);
			v.vertex = mul(v.vertex, m);
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			//o.Emission = c.rgb;
			o.Albedo = c.rgb;//fixed3(0, 0, 0);
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Transparent/Diffuse"
}
