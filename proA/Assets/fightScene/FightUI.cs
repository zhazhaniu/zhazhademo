﻿using UnityEngine;
using System.Collections;

public class FightUI : MonoBehaviour {

    public GameObject[] pets = null;
    public GameObject[] enymies = null;
    public GameObject dialogue = null;
    public GameObject winPanel = null;
    public GameObject fightGo = null;
    public float lastAttack = 0f;

    public int enemyHp = 2;

	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void onAttack()
    {
		if (pets != null )
        {
            if (Time.time - lastAttack < 2f && lastAttack > 0)
            {
                return;
            }

            enemyHp -= 1;
            lastAttack = Time.time;

			for (int i = 0; i < pets.Length; ++i) 
			{
				if (i >= enymies.Length) 
				{
					break;	
				}
				var target = enymies[i];
				var behit01 = target.transform.Find ("behit01");
				behit01.gameObject.SetActive (false);
				behit01.gameObject.SetActive (true);
                if (enemyHp >= 0)
                {
                    Invoke("enemyAttack", 2f);
                }
				
			}

			for (int i = 0; i < pets.Length; ++i) 
			{
				var me = pets[i];
				me.GetComponent<Animation> ().enabled = false;
			}
			Invoke("recoverPetAnim", 0.8f);

            if (enemyHp < 0)
            {
                Invoke("onWin", 2f);
            }
        }
    }

	void recoverPetAnim()
	{
		for (int i = 0; i < pets.Length; ++i) 
		{
			var me = pets[i];
			me.GetComponent<Animation> ().enabled = true;
		}
	}

    void enemyAttack()
    {
		if (pets != null)
        {
			for (int i = 0; i < enymies.Length; ++i)
			{
				var target = enymies[i];
				target.GetComponent<Animation> ().Play ("attack1");
			}
			Invoke ("enemyAttack_1", 1f);
        }
    }

    void enemyAttack_1()
    {
		for (int i = 0; i < pets.Length; ++i) 
		{
			var me = pets[i];
			var behit01 = me.transform.Find ("behit01");
			behit01.gameObject.SetActive (false);
			behit01.gameObject.SetActive (true);
		}
		Invoke("enemyAttack_2", 0.3f);
    }

	void enemyAttack_2()
	{
		for (int i = 0; i < enymies.Length; ++i)
		{
			var target = enymies[i];
			target.GetComponent<Animation> ().Play ("stand");
		}
	}

    void onDialogueFinished()
    {
        Debug.Log("fight dialogue finished");
        winPanel.SetActive(true);
    }

    void onWin()
    {
        fightGo.SetActive(false);
        for (int i = 0; i < enymies.Length; ++i)
        {
            var target = enymies[i];
            target.SetActive(false);
        }
        Invoke("onWinDialogue", 1f);
    }

    void onWinDialogue()
    {
        var dlg = dialogue.GetComponent<ShowDialogue>();
        dlg.currentIndex = 6;
        dlg.onFinished = onDialogueFinished;
        dialogue.SetActive(true);
        dlg.showCurrentDialogue();
    }

    public void onNextScene()
    {
        Application.LoadLevel(3);
    }
}
